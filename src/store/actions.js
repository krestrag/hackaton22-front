const axios = require('axios').default;
const config = require('../config') 

export default{
      async getListingCrypto(ctx){
        const response = await axios.get(`http://localhost:3000/api/listing`);
        ctx.commit('setListingCrypto', response.data)
      },
      async getNewCrypto(ctx){
        const response = await axios.get(`http://localhost:3000/api/recentlyAdded`);
        ctx.commit('setNewCrypto', response.data)
      },
      async getGainersCrypto(ctx){
        const response = await axios.get(`http://localhost:3000/api/gainers`);
        ctx.commit('setGainers', response.data)
      },
      async getLosersCrypto(ctx){
        const response = await axios.get(`http://localhost:3000/api/losers`);
        ctx.commit('setLosers', response.data)
      },
}