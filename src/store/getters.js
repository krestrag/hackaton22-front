export default {
    getCryptoLisiting: state => state.cryptoLisiting,
    getNewCrypto: state => state.cryptoNew,
    getCryptoGainers: state => state.cryptoGainers,
    getLosersCrypto: state => state.cryptoLosers,
}
