import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MainPage',
    component: () => import('../views/MainPage.vue'),
    meta: {
      guest: true
    }
  }, 
  {
    path: '/coinInfo',
    name: 'CoinInfo',
    component: () => import('../views/cryptoInfo.vue'),
    meta: {
      guest: true
    }
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: () => import('../views/LoginPage.vue'),
    meta: {
      guest: true
    }
  },
  {
    path: '/reg',
    name: 'RegisterView',
    component: () => import('../views/RegPage.vue'),
    meta: {
      guest: true
    }
  },
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) 
//   {
//     if (localStorage.getItem('jwt') == null) 
//     {
//       next({
//         path: '/login',
//         params: { nextUrl: to.fullPath }
//       })
//     } else {
//       let user = JSON.parse(localStorage.getItem('user'))
//       if (to.matched.some(record => record.meta.is_admin)) 
//       {
//         if (user.rows[0].role == 1) {
//           next()
//         } else {
//           next({ name: 'MainPage' })
//         }
//       } else {
//         next()
//       }
//     }
//   } else if (to.matched.some(record => record.meta.guest)) 
//   {
//     if (localStorage.getItem('jwt') == null) 
//     {
//       next()
//     }
//     else if(to.path == '/login' || to.path == '/reg'){
//       next('/')
//     }
//      else {
//       next()
//     }
//   } else 
//   {
//     next()
//   }
// })

export default router
